﻿using DotNetTask11.DatabaseConnection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DotNetTask11
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        /*Different functions for querys on database*/
         
        private void syncDatabase_Click(object sender, EventArgs e)
        {
            datagridCharacters.DataSource = SqlListAll.readCharacters();
        }

       
        private void deleteItemBtn_Click(object sender, EventArgs e)
        {
            SqlDelete.deleteCharacter(datagridCharacters.CurrentRow.Cells["ID"].Value.ToString());
            datagridCharacters.DataSource = SqlListAll.readCharacters();

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            
            int columnIndex=(datagridCharacters.CurrentCell.ColumnIndex);
            string columnName = datagridCharacters.Columns[columnIndex].Name;

            string input2 = datagridCharacters.CurrentRow.Cells["ID"].Value.ToString();
            Debug.WriteLine(columnName + "   " + input2);
            SqlUpdate.update(columnName, input2, TBoxNewValue.Text);
            datagridCharacters.DataSource = SqlListAll.readCharacters();

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void btnMainPage_Click(object sender, EventArgs e)
        {
            this.Hide();
            var form1 = new Form1();
            form1.Show();
        }

        
    }
}
