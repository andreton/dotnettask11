﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;

namespace DotNetTask11.DatabaseConnection
{
    class SqlInsert
    {
       
        public static void insertDataToTable(string name, string type, int hp, int mana, int armor, string description, string ability)
        {
            /*Using my local computers connection string*/
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = @"PC7372\SQLEXPRESS";
            builder.InitialCatalog = "DotNetTask11";
            builder.IntegratedSecurity = true;
            try
            {

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    string sql = "INSERT INTO Characters(Name,Type,HP,Mana,Armor,Description,Ability) VALUES(@Name,@Type,@HP,@Mana,@Armor,@Description,@Ability)";
                    connection.Open();
                    Debug.WriteLine("Opening connection....\n");
                    string Name = name;
                    string Type = type;
                    int HP = hp;
                    int Mana = mana;
                    int Armor = armor;
                    string Description = description;
                    string Ability = ability;
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@Name", Name);
                        command.Parameters.AddWithValue("@Type", Type);
                        command.Parameters.AddWithValue("@HP", HP);
                        command.Parameters.AddWithValue("@Mana", Mana);
                        command.Parameters.AddWithValue("@Armor", Armor);
                        command.Parameters.AddWithValue("@Description", Description);
                        command.Parameters.AddWithValue("@Ability", Ability);
                        command.ExecuteNonQuery();
                        Debug.WriteLine("It is added");
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Could not add the chracter to the database, please check the following error message");
                Debug.WriteLine(e.Message);
            }

        }

    }
  }

