﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;

namespace DotNetTask11.DatabaseConnection
{
    class SqlListAll
    {
        public static DataTable readCharacters()
        {
            SqlCommand cmd;
            SqlConnection con;
            SqlDataAdapter da;
            DataTable dataTable = new DataTable();
            /*Connection string for connection to the database*/ 
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = @"PC7372\SQLEXPRESS";
            builder.InitialCatalog = "DotNetTask11";
            builder.IntegratedSecurity = true;
            try
            {

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    Debug.WriteLine("Opening connection....\n");
                    string sql = "SELECT * FROM Characters";
                    SqlDataAdapter SDA = new SqlDataAdapter(sql, connection);
                    SDA.Fill(dataTable);
                    return dataTable;
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine("Can't read the data from the database, please read the following error message: ");
                Debug.WriteLine(e.StackTrace);
                return null;
            }
        }
    }
}
