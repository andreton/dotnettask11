﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;

namespace DotNetTask11.DatabaseConnection
{
    class SqlUpdate
    {
        public static void update(string typeIdentifier,string IdIdentifier, string newValue)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = @"PC7372\SQLEXPRESS";
            builder.InitialCatalog = "DotNetTask11";
            builder.IntegratedSecurity = true;
            string sql = "";
            try 
            {
                switch(typeIdentifier){
                    case "Type":
                        sql += "Update Characters SET Type = @newValue Where ID= @IdIdentifier";
                        break;
                    case "Name":
                        sql += "Update Characters SET Name = @newValue Where ID= @IdIdentifier"; break;
                        
                    case "HP":
                        sql += "Update Characters SET HP = @newValue Where ID= @IdIdentifier"; break;
                       
                    case "Mana":
                        sql += "Update Characters SET Mana = @newValue Where ID= @IdIdentifier"; break;
                        break;
                    case "Armor":
                        sql += "Update Characters SET Armor = @newValue Where ID= @IdIdentifier"; break;
                        break;
                    default:
                        break;
                }

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                   
                    connection.Open();
                    Debug.WriteLine(sql);
                    Debug.WriteLine("Opening connection....\n");

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@newValue", newValue);
                        command.Parameters.AddWithValue("@IdIdentifier", IdIdentifier);
                        command.ExecuteNonQuery();
                        Debug.WriteLine("Sucsessfully updated");
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }

        }
    }
}
