﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;

namespace DotNetTask11.DatabaseConnection
{
    class SqlDelete
    {
        public static void deleteCharacter(string deleteId)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = @"PC7372\SQLEXPRESS";
            builder.InitialCatalog = "DotNetTask11";
            builder.IntegratedSecurity = true;
            try
            {

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    string sql = "Delete From Characters Where ID=" + deleteId; //Using ID given by the function header
                    connection.Open();
                    Debug.WriteLine("Opening connection....\n");
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.ExecuteNonQuery();
                        Debug.WriteLine("It is deleted");
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Could not delete, please read the following message");
                Debug.WriteLine(e.Message);
            }

        }

    }
}
