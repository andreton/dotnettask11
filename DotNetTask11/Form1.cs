﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.CompilerServices;
using DotNetTask11.DatabaseConnection;


namespace DotNetTask11
{
    public partial class Form1 : Form
    {
        List<Character> listCharacters = new List<Character>(); //listCharacters to store the created characters chronologically
        Boolean mainClassChosen = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            InitalizeApplication();
        }
        public void InitalizeApplication() //packing the initalzation in seperate function, so it can be called when we need to reset the form
        {
            List<Character> charactersLoad = new List<Character>(); //A short List to populate the combobox with 3 items: Mage, warrior, thief
            Character mage = new Mage("Mage");
            charactersLoad.Add(mage);
            Character warrior = new Warrior("Warrior");
            charactersLoad.Add(warrior);
            Character thief = new Thief("Thief");
            charactersLoad.Add(thief);
            this.CboxchooseClass.DisplayMember = null;
            this.CboxchooseClass.ValueMember = "name";
            this.CboxchooseClass.DataSource = charactersLoad; //Filing the combobox with three main classes the user can choose
            textBox1.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e) // Button for choosing main class
        {
            var chosenCharacter =(Character) CboxchooseClass.SelectedItem; //Getting the chosen character from combobox
            chosenCaracterAddSubclass(chosenCharacter.name); //Use the name attribute, in this case this gives us the type of main class chosen, Mage, Warrior or Thief
            lblDescriptionSubClass.Visible=true; // Makes an invisible blocj 
        }
        private void chosenCaracterAddSubclass(string whichClass)
        {
            switch (whichClass) // A switch statement to display one of the three boxes based on main class
            {                   //The user then get to see the differnt subclasses, groupboxes are stacked on top of each other and defaut visibility =false
                case "Mage":
                    groupBox1.Visible = true;
                    groupBox2.Visible = false;
                    groupBox3.Visible = false;
                    try
                    {
                        pictureCharacter.ImageLocation = @"C:\Users\anmyhr\source\repos\DotNetTask11\DotNetTask11\Pictures\Mage.png"; //Uploading some awesome pixel art
                        pictureCharacter.Load();
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine("Could not open picture file" + e.Message);
                    }
                   
                    break;
                case "Warrior":
                    groupBox1.Visible = false;
                    groupBox2.Visible = true;
                    groupBox3.Visible = false;
                    try
                    {
                        pictureCharacter.ImageLocation = @"C:\Users\anmyhr\source\repos\DotNetTask11\DotNetTask11\Pictures\Warrior.png"; //Uploading some awesome pixel art
                        pictureCharacter.Load();
                    }
                    catch(Exception e)
                    {
                        Debug.WriteLine("Could not open picture file" + e.Message);
                    }
                  
                    break;

                case "Thief":
                    groupBox1.Visible = false;
                    groupBox2.Visible = false;
                    groupBox3.Visible = true;
                    try
                    {
                        pictureCharacter.ImageLocation = @"C:\Users\anmyhr\source\repos\DotNetTask11\DotNetTask11\Pictures\Thief.png"; //Uploading some awesome pixel art
                        pictureCharacter.Load();
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine("Could not open picture file" + e.Message);
                    }

                    break;
            }

        }

        //There are two subclasses for each main class: 

        //These functions display the different attributes based on which class you choose

        //Buttons used to specify which subclass you want
        private void warriorButton_Click(object sender, EventArgs e)
        {
            btnCreate.Visible = true; // Button to create the character becomes visible
            btnStartOver.Visible = true; //Button for starting over becomes visible
            string returnClassString; //A string that returns which character that is created 
            lblPointsLeft.Text = "10"; //Point used to create your character
            /*Based on which character choosen you get info about special ability*/
            if (goblinButton.Checked == true) 
            {
                lblspcAbility.Text = "strength";
                defaultConstr.Visible = true;
                extraConstruct.Visible = true;
                returnClassString = "goblin";
            }
            else if (squireButton.Checked == true)
            {
                lblspcAbility.Text = "speed";
                defaultConstr.Visible = true;
                extraConstruct.Visible = true;
                returnClassString = "squire";
            }
            else if (warriorNoSubclassButton.Checked == true)
            {
                lblPointsLeft.Text = "15"; //With no subclass you get no special ability, but extra points instead
                mainClassChosen = true;
                lblspcAbility.Text = "None";
                defaultConstr.Visible = true;
                extraConstruct.Visible = false;
                returnClassString = "thief";
            }
            else
            {
                MessageBox.Show("You must choose a class"); //You must choose a class
            }
        }
        private void mageButton_Click(object sender, EventArgs e)
        {
            btnCreate.Visible = true;
            btnStartOver.Visible = true;
            string returnClassString;
            lblPointsLeft.Text = "10";
            

            if (darkWizardButton.Checked == true)
            {
                lblspcAbility.Text = "Fireball";
                defaultConstr.Visible = true;
                extraConstruct.Visible = true;
                returnClassString = "darkwizard";   
            }
            else if (wizardButton.Checked == true)
            {
                lblspcAbility.Text = "Waterball";
                defaultConstr.Visible = true;
                extraConstruct.Visible = true;
                returnClassString = "squire";
            }
            else if (noSubclassMageButton.Checked == true)
            {
                lblPointsLeft.Text = "15";
                mainClassChosen = true;
                lblspcAbility.Text = "None";
                defaultConstr.Visible = true;
                extraConstruct.Visible = false;
                returnClassString = "thief";
            }
            else
            {
                MessageBox.Show("You must choose a class"); //You must choose a class
            }

        }
        private void thiefButton_Click(object sender, EventArgs e)
        {
            btnCreate.Visible = true;
            btnStartOver.Visible = true;
            lblPointsLeft.Text = "10";
            string returnClassString; //
            if (burglarButton.Checked == true)
            {
                lblspcAbility.Text = "Stealyh";
                defaultConstr.Visible = true;
                extraConstruct.Visible = true;

            }
            else if (rogueButton.Checked == true)
            {
                lblspcAbility.Text = "Cunningness";
                defaultConstr.Visible = true;
                extraConstruct.Visible = true;
                returnClassString = "rogue";
            }
            else if (noSubclassThiefButton.Checked == true)
            {
                mainClassChosen = true;
                lblPointsLeft.Text = "15";
                lblspcAbility.Text = "None";
                defaultConstr.Visible = true;
                extraConstruct.Visible = false;
                returnClassString = "thief";
            }
            else
            {
                MessageBox.Show("You must choose a class"); //You must choose a class
                return; // Step out of the program
            }

        }
        /*This piece of code calculates the left skill points for the different class, based on HP, Mana and armor*/
        private void HPUpDown_ValueChanged(object sender, EventArgs e)
        {
            int newValue = totalPointsLeft((int)HPUpDown.Value, (int)manaUpDown.Value, (int)armorUpDown.Value);
            lblPointsLeft.Text = newValue.ToString();
        }

        private void manaUpDown_ValueChanged(object sender, EventArgs e)
        {
            int newValue = totalPointsLeft((int)HPUpDown.Value, (int)manaUpDown.Value, (int)armorUpDown.Value);
            lblPointsLeft.Text = newValue.ToString();
        }

        private void armorUpDown_ValueChanged(object sender, EventArgs e)
        {
            int newValue = totalPointsLeft((int)HPUpDown.Value, (int)manaUpDown.Value, (int)armorUpDown.Value);
            lblPointsLeft.Text = newValue.ToString();

        }
        public int totalPointsLeft(int HpInput, int ManaInput, int ArmorInput)
        {
            if (mainClassChosen)
            {
                return 15 - HpInput - ManaInput - ArmorInput;
            }
            else
            {
                return 10 - HpInput - ManaInput - ArmorInput;
            }

        }
        /*Stop on functions used for calculating skill points left*/

        private void btnStartOver_Click(object sender, EventArgs e) //Resets the application and all forms if user wants to start over
        {
            this.Controls.Clear();
            this.InitializeComponent();
            InitalizeApplication();
        }

        private void btnCharacterpage_Click(object sender, EventArgs e) //Goes to page for listing characters 
        {
            this.Hide();
            var form2 = new Form2();
            form2.Show();
        }

        private void btnCreate_Click(object sender, EventArgs e) /* This long function makes the given character based on which radiobutton that is pressed.
        I am not to happy with this function, it seems a bit to long and messy, but i couldn't quite find a better solution. Happy to take some improvements (but it does work)!*/
        {
            textBox1.Visible = true;
            if (lblPointsLeft.Text != 0.ToString())
            {
                MessageBox.Show("You must use the exact number of skillpoints");
                return;
            }
            if (NameConst.Text == "")
            {
                MessageBox.Show("You must enter a name");
                return;
            }
            if (goblinButton.Checked) {
                Character goblin = new Goblin(NameConst.Text, (int)HPUpDown.Value, (int)manaUpDown.Value, (int)armorUpDown.Value);
                listCharacters.Add(goblin);
                textBox1.Text = showdescription(goblin);

            }
            else if (squireButton.Checked)
            {
                Character squire = new Squire(NameConst.Text, (int)HPUpDown.Value, (int)manaUpDown.Value, (int)armorUpDown.Value);
                listCharacters.Add(squire);
                textBox1.Text = showdescription(squire);
            }
            else if (warriorNoSubclassButton.Checked)
            {

                Character warrior = new Warrior(NameConst.Text, (int)HPUpDown.Value, (int)manaUpDown.Value, (int)armorUpDown.Value);
                listCharacters.Add(warrior);
                textBox1.Text = showdescription(warrior);
            }
            else if (darkWizardButton.Checked)
            {
                Character darkwizard = new DarkWizard(NameConst.Text, (int)HPUpDown.Value, (int)manaUpDown.Value, (int)armorUpDown.Value);
                listCharacters.Add(darkwizard);
                textBox1.Text = showdescription(darkwizard);
            }
            else if (wizardButton.Checked)
            {
                Character wizard = new Wizard(NameConst.Text, (int)HPUpDown.Value, (int)manaUpDown.Value, (int)armorUpDown.Value);
                listCharacters.Add(wizard);
                textBox1.Text = showdescription(wizard);
            }
            else if (noSubclassMageButton.Checked)
            {
                Character mage = new Mage(NameConst.Text, (int)HPUpDown.Value, (int)manaUpDown.Value, (int)armorUpDown.Value);
                listCharacters.Add(mage);
                textBox1.Text = showdescription(mage);

            }
            else if (rogueButton.Checked)
            {
                Character rogue = new Rogue(NameConst.Text, (int)HPUpDown.Value, (int)manaUpDown.Value, (int)armorUpDown.Value);
                listCharacters.Add(rogue);
                textBox1.Text = showdescription(rogue);
            }
            else if (burglarButton.Checked)
            {
                Character burglar = new Burglar(NameConst.Text, (int)HPUpDown.Value, (int)manaUpDown.Value, (int)armorUpDown.Value);
                listCharacters.Add(burglar);
                textBox1.Text = showdescription(burglar);

            }
            else if (noSubclassThiefButton.Checked)
            {
                Character thief = new Thief(NameConst.Text, (int)HPUpDown.Value, (int)manaUpDown.Value, (int)armorUpDown.Value);
                listCharacters.Add(thief);
                textBox1.Text = showdescription(thief);
            }
            Character c=listCharacters.Last(); //Getting the last added character in the list in case more are added
            displayCharactertextBox.Text = "Your character is created, good luck on your quest!\n" +
            c.ToString(); //Displaying the character with the overriden ToString method for each class


            /*Then we write the character to the default location calling the write to file function*/
            WriteToFile.writeTxt(c.ToString()); // writes to the default folder inside the project folder

            /*Also inserting the data to the database, the file for the table and the database is in the folder "ScriptForDatabase*/
            SqlInsert.insertDataToTable(c.name, c.characterClassType(), c.HP, c.mana, c.armorRating, c.description(),c.getSpecialAbility());
                      
        }
        private string showdescription(Character character) //Function for printing description
        {
            return character.description();
        }

        
    }
}
