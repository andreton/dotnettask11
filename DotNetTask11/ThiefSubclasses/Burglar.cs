﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace DotNetTask11
{
    class Burglar :Thief
    {
        public string specialAbility { get; set; }
        public Burglar(string name, int HP, int mana, int armorRating) : base(name, HP, mana, armorRating)
        {
            this.specialAbility = "cunningnes";
        }
        public override string getSpecialAbility()
        {
            return specialAbility;
        }
        public override string ToString()
        {
            return "Name:" + this.name + " HP: " + this.HP + " Mana: " + this.mana + " Armor: " + this.armorRating + " Special ability: " + this.specialAbility;
        }
        public override string description()
        {
            return "The burglar is a subclass of thief, he robs you and has an extra ability";
        }
        public override string characterClassType(){
            return "Burglar";
        }
         
    }
}
