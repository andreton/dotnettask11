﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask11
{
    
    class Rogue :Thief
    {
        public string specialAbility { get; set; }
        public Rogue(string name, int HP, int mana, int armorRating) : base(name, HP, mana, armorRating)
        {
            this.specialAbility = "stealth";
        }
        public override string getSpecialAbility()
        {
            return specialAbility;
        }
        public override string ToString()
        {
            return "Name:" + this.name + " HP: " + this.HP + " Mana: " + this.mana + " Armor: " + this.armorRating + " Special ability: " + this.specialAbility;
        }
        public override string description()
        {
            return "The rogu is a subclass of mage and has an extra secret ability, watch out";
        }
        public override string characterClassType(){
            return "Rogue";
        }


    }
}
