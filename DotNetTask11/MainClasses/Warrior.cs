﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask11
{
    class Warrior : Character {
      
        public Warrior(string name) : base(name)
        {
            this.name = name;
        }

        public Warrior(string name, int HP, int mana, int armorRating) : base(name, HP, mana, armorRating)
    {
            this.name = name;
            this.HP = HP;
            this.mana = mana;
            this.armorRating = armorRating;
        }
        public override string ToString()
        {
            return "Name:" + this.name + " HP: " + this.HP + " Mana: " + this.mana + " Armor: " + this.armorRating;
        }
        public override string description()
        {
            return "The warrior is good with the sword, but dumb as a bread(Norwegian saying)";
        }
        public override string characterClassType(){
            return "Warrior";
        }

        public override string getSpecialAbility()
        {
            return "No special ability";
        }
        public override void attack()//Basic attack method
        {
            Console.WriteLine("Attack!! You lose 50HP");
        }

        public override void movement()
        {
            Console.WriteLine("Moved away, the attack missed");
        }
    }
}
