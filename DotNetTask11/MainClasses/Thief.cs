﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask11
{
    class Thief :Character
    {
       
        public Thief(string name) : base(name)
        {
            this.name = name;
        }

        public Thief(string name, int HP, int mana, int armorRating) : base(name, HP, mana, armorRating)
        {
            this.name = name;
            this.HP = HP;
            this.mana = mana;
            this.armorRating = armorRating;
        }
        public override string ToString()
        {
            return "Name:" + this.name + " HP: " + this.HP + " Mana: " + this.mana + " Armor: " + this.armorRating;
        }
        public override string description() // Desciription of character
        {
            return "The thief is cunning and can easily steal your stuff";
        }
        public override string characterClassType(){
            return "Thief";
        }

        public override string getSpecialAbility()
        {
            return "No special ability";
        }
        public override void attack()
        {
            Console.WriteLine("Attack!! You lose 50HP");
        }

        public override void movement()
        {
            Console.WriteLine("Moved away, the attack missed");
        }
    }
}
