﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask11
{
    class Mage : Character
    {
        public Mage(string name):base( name)
        {
            this.name = name;
        }
        //2 default constructors for mage
        public Mage(string name, int HP,int mana, int armorRating) : base(name, HP, mana, armorRating)
        {
            this.name = name;
            this.HP = HP;
            this.mana = mana;
            this.armorRating = armorRating;
        }
        public override string ToString()
        {
            return "Name:" + this.name + " HP: " + this.HP + " Mana: " + this.mana + " Armor: " + this.armorRating;
        }
        public override string description() //Description of the character
        {
            return "The mage is a master of might and magic";
        }
        public override string characterClassType(){ //Return class type
            return "Mage";
        }

        public override string getSpecialAbility() //These main classes don't have any special ability
        {
            return "No special ability";
        }
        public override void attack()
        {
            Console.WriteLine("Attack!! You lose 50HP");
        }

        public override void movement()
        {
            Console.WriteLine("Moved away, the attack missed");
        }
    }
}
