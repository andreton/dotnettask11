﻿using DotNetTask11.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask11
{
    /*The superclass that all classes extends from, implements some basic interfaces that is needed for te other characters*/
    abstract class Character : IAttack, Imove, IDescription, ICharacterClass
    {
       
        public string name { get; set; }
        public int HP { get; set; }
        public int mana { get; set; }
        public int armorRating { get; set; }

        protected Character(string name)
        {
            this.name = name;
        }
        protected Character(string name, int HP, int mana, int armorRating)
        {
            this.name = name;
            this.HP = HP;
            this.mana = mana;
            this.armorRating = armorRating;
        }

        string toString() {
            return "Name:" + name + " HP: " + HP + " Mana: " + mana + " Armor: " + armorRating;
        }

        public abstract void attack();

        public abstract void movement();

        public abstract string description(); //Abstract methods for other to inherit

        public abstract string characterClassType(); //Abstract methods for other to inherit

        public abstract string getSpecialAbility(); //Abstract methods for other to inherit



        /*
        public static bool checkUserInputMainClass(string name, int hp, int mana, int armor)
        {
            string[] userInput = new string[4];
            if (string.IsNullOrEmpty(name) || int.IsNullOrEmpty(hp) || string.IsNullOrEmpty(mana) || string.IsNullOrEmpty(armor))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public static bool checkUserInputSubClass(string name, string hp, string mana, string armor, string extraAbility)
        {
            string[] userInput = new string[4];
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(hp) || string.IsNullOrEmpty(mana) || string.IsNullOrEmpty(armor) || string.IsNullOrEmpty(extraAbility))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        */
    }
}
