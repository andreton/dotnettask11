﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DotNetTask11
{
    class WriteToFile
    {
        public static void writeTxt(string inputString)
        {
            try
            {
                StreamWriter sw = new StreamWriter(@"output.txt", true); //Writes file to the source folder in txt format
                sw.WriteLine(inputString);
                sw.Flush();
                sw.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Not able to print" + e.StackTrace);
            }
        }
    }
}