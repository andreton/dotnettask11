﻿namespace DotNetTask11
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.CboxchooseClass = new System.Windows.Forms.ComboBox();
            this.lblDescriptionSubClass = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.mageButton = new System.Windows.Forms.Button();
            this.noSubclassMageButton = new System.Windows.Forms.RadioButton();
            this.darkWizardButton = new System.Windows.Forms.RadioButton();
            this.wizardButton = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.warriorButton = new System.Windows.Forms.Button();
            this.goblinButton = new System.Windows.Forms.RadioButton();
            this.warriorNoSubclassButton = new System.Windows.Forms.RadioButton();
            this.squireButton = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.thiefButton = new System.Windows.Forms.Button();
            this.noSubclassThiefButton = new System.Windows.Forms.RadioButton();
            this.rogueButton = new System.Windows.Forms.RadioButton();
            this.burglarButton = new System.Windows.Forms.RadioButton();
            this.defaultConstr = new System.Windows.Forms.GroupBox();
            this.lblPointsLeft = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.armorUpDown = new System.Windows.Forms.NumericUpDown();
            this.manaUpDown = new System.Windows.Forms.NumericUpDown();
            this.HPUpDown = new System.Windows.Forms.NumericUpDown();
            this.NameConst = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.extraConstruct = new System.Windows.Forms.GroupBox();
            this.lblspcAbility = new System.Windows.Forms.Label();
            this.lblSpecialAbility = new System.Windows.Forms.Label();
            this.btnCreate = new System.Windows.Forms.Button();
            this.displayCharactertextBox = new System.Windows.Forms.RichTextBox();
            this.pictureCharacter = new System.Windows.Forms.PictureBox();
            this.btnStartOver = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnCharacterpage = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.defaultConstr.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.armorUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.manaUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HPUpDown)).BeginInit();
            this.extraConstruct.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureCharacter)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Crimson;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(216, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(408, 41);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome to Dungeon master";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(208, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Choose your main class: ";
            // 
            // CboxchooseClass
            // 
            this.CboxchooseClass.FormattingEnabled = true;
            this.CboxchooseClass.Location = new System.Drawing.Point(306, 106);
            this.CboxchooseClass.Name = "CboxchooseClass";
            this.CboxchooseClass.Size = new System.Drawing.Size(182, 33);
            this.CboxchooseClass.TabIndex = 2;
            // 
            // lblDescriptionSubClass
            // 
            this.lblDescriptionSubClass.AutoSize = true;
            this.lblDescriptionSubClass.Location = new System.Drawing.Point(37, 177);
            this.lblDescriptionSubClass.Name = "lblDescriptionSubClass";
            this.lblDescriptionSubClass.Size = new System.Drawing.Size(706, 25);
            this.lblDescriptionSubClass.TabIndex = 3;
            this.lblDescriptionSubClass.Text = "Choose your subclass (optional) If you choose a subclass you get an extra special" +
    " ability";
            this.lblDescriptionSubClass.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(493, 104);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 34);
            this.button1.TabIndex = 4;
            this.button1.Text = "Choose";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.mageButton);
            this.groupBox1.Controls.Add(this.noSubclassMageButton);
            this.groupBox1.Controls.Add(this.darkWizardButton);
            this.groupBox1.Controls.Add(this.wizardButton);
            this.groupBox1.Location = new System.Drawing.Point(37, 238);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(423, 150);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Mage";
            this.groupBox1.Visible = false;
            // 
            // mageButton
            // 
            this.mageButton.Location = new System.Drawing.Point(152, 83);
            this.mageButton.Name = "mageButton";
            this.mageButton.Size = new System.Drawing.Size(112, 34);
            this.mageButton.TabIndex = 3;
            this.mageButton.Text = "Continue";
            this.mageButton.UseVisualStyleBackColor = true;
            this.mageButton.Click += new System.EventHandler(this.mageButton_Click);
            // 
            // noSubclassMageButton
            // 
            this.noSubclassMageButton.AutoSize = true;
            this.noSubclassMageButton.Location = new System.Drawing.Point(240, 25);
            this.noSubclassMageButton.Name = "noSubclassMageButton";
            this.noSubclassMageButton.Size = new System.Drawing.Size(132, 29);
            this.noSubclassMageButton.TabIndex = 2;
            this.noSubclassMageButton.TabStop = true;
            this.noSubclassMageButton.Text = "No subclass";
            this.noSubclassMageButton.UseVisualStyleBackColor = true;
            // 
            // darkWizardButton
            // 
            this.darkWizardButton.AutoSize = true;
            this.darkWizardButton.Location = new System.Drawing.Point(104, 21);
            this.darkWizardButton.Name = "darkWizardButton";
            this.darkWizardButton.Size = new System.Drawing.Size(129, 29);
            this.darkWizardButton.TabIndex = 1;
            this.darkWizardButton.TabStop = true;
            this.darkWizardButton.Text = "DarkWizard";
            this.darkWizardButton.UseVisualStyleBackColor = true;
            // 
            // wizardButton
            // 
            this.wizardButton.AutoSize = true;
            this.wizardButton.Location = new System.Drawing.Point(6, 21);
            this.wizardButton.Name = "wizardButton";
            this.wizardButton.Size = new System.Drawing.Size(92, 29);
            this.wizardButton.TabIndex = 0;
            this.wizardButton.TabStop = true;
            this.wizardButton.Text = "Wizard";
            this.wizardButton.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.warriorButton);
            this.groupBox2.Controls.Add(this.goblinButton);
            this.groupBox2.Controls.Add(this.warriorNoSubclassButton);
            this.groupBox2.Controls.Add(this.squireButton);
            this.groupBox2.Location = new System.Drawing.Point(37, 238);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(429, 150);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Warrior";
            this.groupBox2.Visible = false;
            // 
            // warriorButton
            // 
            this.warriorButton.Location = new System.Drawing.Point(152, 83);
            this.warriorButton.Name = "warriorButton";
            this.warriorButton.Size = new System.Drawing.Size(112, 34);
            this.warriorButton.TabIndex = 3;
            this.warriorButton.Text = "Continue";
            this.warriorButton.UseVisualStyleBackColor = true;
            this.warriorButton.Click += new System.EventHandler(this.warriorButton_Click);
            // 
            // goblinButton
            // 
            this.goblinButton.AutoSize = true;
            this.goblinButton.Location = new System.Drawing.Point(104, 31);
            this.goblinButton.Name = "goblinButton";
            this.goblinButton.Size = new System.Drawing.Size(89, 29);
            this.goblinButton.TabIndex = 1;
            this.goblinButton.TabStop = true;
            this.goblinButton.Text = "Goblin";
            this.goblinButton.UseVisualStyleBackColor = true;
            // 
            // warriorNoSubclassButton
            // 
            this.warriorNoSubclassButton.AutoSize = true;
            this.warriorNoSubclassButton.Location = new System.Drawing.Point(214, 31);
            this.warriorNoSubclassButton.Name = "warriorNoSubclassButton";
            this.warriorNoSubclassButton.Size = new System.Drawing.Size(132, 29);
            this.warriorNoSubclassButton.TabIndex = 2;
            this.warriorNoSubclassButton.TabStop = true;
            this.warriorNoSubclassButton.Text = "No subclass";
            this.warriorNoSubclassButton.UseVisualStyleBackColor = true;
            // 
            // squireButton
            // 
            this.squireButton.AutoSize = true;
            this.squireButton.Location = new System.Drawing.Point(7, 31);
            this.squireButton.Name = "squireButton";
            this.squireButton.Size = new System.Drawing.Size(87, 29);
            this.squireButton.TabIndex = 0;
            this.squireButton.TabStop = true;
            this.squireButton.Text = "Squire";
            this.squireButton.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.thiefButton);
            this.groupBox3.Controls.Add(this.noSubclassThiefButton);
            this.groupBox3.Controls.Add(this.rogueButton);
            this.groupBox3.Controls.Add(this.burglarButton);
            this.groupBox3.Location = new System.Drawing.Point(43, 238);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(423, 150);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Thief";
            this.groupBox3.Visible = false;
            // 
            // thiefButton
            // 
            this.thiefButton.Location = new System.Drawing.Point(152, 83);
            this.thiefButton.Name = "thiefButton";
            this.thiefButton.Size = new System.Drawing.Size(112, 34);
            this.thiefButton.TabIndex = 3;
            this.thiefButton.Text = "Continue";
            this.thiefButton.UseVisualStyleBackColor = true;
            this.thiefButton.Click += new System.EventHandler(this.thiefButton_Click);
            // 
            // noSubclassThiefButton
            // 
            this.noSubclassThiefButton.AutoSize = true;
            this.noSubclassThiefButton.Location = new System.Drawing.Point(265, 25);
            this.noSubclassThiefButton.Name = "noSubclassThiefButton";
            this.noSubclassThiefButton.Size = new System.Drawing.Size(132, 29);
            this.noSubclassThiefButton.TabIndex = 2;
            this.noSubclassThiefButton.TabStop = true;
            this.noSubclassThiefButton.Text = "No subclass";
            this.noSubclassThiefButton.UseVisualStyleBackColor = true;
            // 
            // rogueButton
            // 
            this.rogueButton.AutoSize = true;
            this.rogueButton.Location = new System.Drawing.Point(146, 25);
            this.rogueButton.Name = "rogueButton";
            this.rogueButton.Size = new System.Drawing.Size(88, 29);
            this.rogueButton.TabIndex = 1;
            this.rogueButton.TabStop = true;
            this.rogueButton.Text = "Rogue";
            this.rogueButton.UseVisualStyleBackColor = true;
            // 
            // burglarButton
            // 
            this.burglarButton.AutoSize = true;
            this.burglarButton.Location = new System.Drawing.Point(21, 25);
            this.burglarButton.Name = "burglarButton";
            this.burglarButton.Size = new System.Drawing.Size(93, 29);
            this.burglarButton.TabIndex = 0;
            this.burglarButton.TabStop = true;
            this.burglarButton.Text = "Burglar";
            this.burglarButton.UseVisualStyleBackColor = true;
            // 
            // defaultConstr
            // 
            this.defaultConstr.Controls.Add(this.lblPointsLeft);
            this.defaultConstr.Controls.Add(this.label9);
            this.defaultConstr.Controls.Add(this.armorUpDown);
            this.defaultConstr.Controls.Add(this.manaUpDown);
            this.defaultConstr.Controls.Add(this.HPUpDown);
            this.defaultConstr.Controls.Add(this.NameConst);
            this.defaultConstr.Controls.Add(this.label8);
            this.defaultConstr.Controls.Add(this.label7);
            this.defaultConstr.Controls.Add(this.label6);
            this.defaultConstr.Controls.Add(this.label5);
            this.defaultConstr.Location = new System.Drawing.Point(37, 390);
            this.defaultConstr.Name = "defaultConstr";
            this.defaultConstr.Size = new System.Drawing.Size(310, 236);
            this.defaultConstr.TabIndex = 6;
            this.defaultConstr.TabStop = false;
            this.defaultConstr.Visible = false;
            // 
            // lblPointsLeft
            // 
            this.lblPointsLeft.AutoSize = true;
            this.lblPointsLeft.BackColor = System.Drawing.Color.LightSalmon;
            this.lblPointsLeft.Location = new System.Drawing.Point(228, 147);
            this.lblPointsLeft.Name = "lblPointsLeft";
            this.lblPointsLeft.Size = new System.Drawing.Size(22, 25);
            this.lblPointsLeft.TabIndex = 11;
            this.lblPointsLeft.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.LightSalmon;
            this.label9.Location = new System.Drawing.Point(193, 110);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 25);
            this.label9.TabIndex = 10;
            this.label9.Text = "Points Left";
            // 
            // armorUpDown
            // 
            this.armorUpDown.Location = new System.Drawing.Point(90, 196);
            this.armorUpDown.Name = "armorUpDown";
            this.armorUpDown.Size = new System.Drawing.Size(84, 31);
            this.armorUpDown.TabIndex = 9;
            this.armorUpDown.ValueChanged += new System.EventHandler(this.armorUpDown_ValueChanged);
            // 
            // manaUpDown
            // 
            this.manaUpDown.Location = new System.Drawing.Point(90, 147);
            this.manaUpDown.Name = "manaUpDown";
            this.manaUpDown.Size = new System.Drawing.Size(84, 31);
            this.manaUpDown.TabIndex = 9;
            this.manaUpDown.ValueChanged += new System.EventHandler(this.manaUpDown_ValueChanged);
            // 
            // HPUpDown
            // 
            this.HPUpDown.Location = new System.Drawing.Point(90, 90);
            this.HPUpDown.Name = "HPUpDown";
            this.HPUpDown.Size = new System.Drawing.Size(84, 31);
            this.HPUpDown.TabIndex = 9;
            this.HPUpDown.ValueChanged += new System.EventHandler(this.HPUpDown_ValueChanged);
            // 
            // NameConst
            // 
            this.NameConst.Location = new System.Drawing.Point(90, 31);
            this.NameConst.Name = "NameConst";
            this.NameConst.Size = new System.Drawing.Size(150, 31);
            this.NameConst.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(21, 199);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 25);
            this.label8.TabIndex = 3;
            this.label8.Text = "Armor";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 149);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 25);
            this.label7.TabIndex = 2;
            this.label7.Text = "Mana";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 92);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 25);
            this.label6.TabIndex = 1;
            this.label6.Text = "HP";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 25);
            this.label5.TabIndex = 0;
            this.label5.Text = "Name";
            // 
            // extraConstruct
            // 
            this.extraConstruct.Controls.Add(this.lblspcAbility);
            this.extraConstruct.Controls.Add(this.lblSpecialAbility);
            this.extraConstruct.Location = new System.Drawing.Point(368, 390);
            this.extraConstruct.Name = "extraConstruct";
            this.extraConstruct.Size = new System.Drawing.Size(437, 117);
            this.extraConstruct.TabIndex = 6;
            this.extraConstruct.TabStop = false;
            this.extraConstruct.Visible = false;
            // 
            // lblspcAbility
            // 
            this.lblspcAbility.AutoSize = true;
            this.lblspcAbility.BackColor = System.Drawing.Color.White;
            this.lblspcAbility.Location = new System.Drawing.Point(180, 70);
            this.lblspcAbility.Name = "lblspcAbility";
            this.lblspcAbility.Size = new System.Drawing.Size(0, 25);
            this.lblspcAbility.TabIndex = 2;
            // 
            // lblSpecialAbility
            // 
            this.lblSpecialAbility.AutoSize = true;
            this.lblSpecialAbility.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblSpecialAbility.Location = new System.Drawing.Point(152, 27);
            this.lblSpecialAbility.Name = "lblSpecialAbility";
            this.lblSpecialAbility.Size = new System.Drawing.Size(133, 25);
            this.lblSpecialAbility.TabIndex = 1;
            this.lblSpecialAbility.Text = "Special Ability";
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(37, 632);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(346, 59);
            this.btnCreate.TabIndex = 7;
            this.btnCreate.Text = "Create";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Visible = false;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // displayCharactertextBox
            // 
            this.displayCharactertextBox.BackColor = System.Drawing.Color.Salmon;
            this.displayCharactertextBox.Location = new System.Drawing.Point(31, 701);
            this.displayCharactertextBox.Name = "displayCharactertextBox";
            this.displayCharactertextBox.Size = new System.Drawing.Size(492, 91);
            this.displayCharactertextBox.TabIndex = 9;
            this.displayCharactertextBox.Text = "";
            // 
            // pictureCharacter
            // 
            this.pictureCharacter.Location = new System.Drawing.Point(503, 249);
            this.pictureCharacter.Name = "pictureCharacter";
            this.pictureCharacter.Size = new System.Drawing.Size(302, 135);
            this.pictureCharacter.TabIndex = 11;
            this.pictureCharacter.TabStop = false;
            // 
            // btnStartOver
            // 
            this.btnStartOver.Location = new System.Drawing.Point(394, 632);
            this.btnStartOver.Name = "btnStartOver";
            this.btnStartOver.Size = new System.Drawing.Size(411, 59);
            this.btnStartOver.TabIndex = 12;
            this.btnStartOver.Text = "Start Over";
            this.btnStartOver.UseVisualStyleBackColor = true;
            this.btnStartOver.Visible = false;
            this.btnStartOver.Click += new System.EventHandler(this.btnStartOver_Click);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.Salmon;
            this.textBox1.Location = new System.Drawing.Point(368, 537);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(437, 89);
            this.textBox1.TabIndex = 13;
            this.textBox1.Visible = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(520, 510);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 25);
            this.label3.TabIndex = 1;
            this.label3.Text = "Description";
            // 
            // btnCharacterpage
            // 
            this.btnCharacterpage.Location = new System.Drawing.Point(539, 701);
            this.btnCharacterpage.Name = "btnCharacterpage";
            this.btnCharacterpage.Size = new System.Drawing.Size(266, 91);
            this.btnCharacterpage.TabIndex = 14;
            this.btnCharacterpage.Text = "To Characters listings";
            this.btnCharacterpage.UseVisualStyleBackColor = true;
            this.btnCharacterpage.Click += new System.EventHandler(this.btnCharacterpage_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LimeGreen;
            this.ClientSize = new System.Drawing.Size(844, 823);
            this.Controls.Add(this.btnCharacterpage);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btnStartOver);
            this.Controls.Add(this.pictureCharacter);
            this.Controls.Add(this.displayCharactertextBox);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.extraConstruct);
            this.Controls.Add(this.defaultConstr);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblDescriptionSubClass);
            this.Controls.Add(this.CboxchooseClass);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Dungeon Master";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.defaultConstr.ResumeLayout(false);
            this.defaultConstr.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.armorUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.manaUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HPUpDown)).EndInit();
            this.extraConstruct.ResumeLayout(false);
            this.extraConstruct.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureCharacter)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox CboxchooseClass;
        private System.Windows.Forms.Label lblDescriptionSubClass;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton darkWizardButton;
        private System.Windows.Forms.RadioButton wizardButton;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton squireButton;
        private System.Windows.Forms.RadioButton goblinButton;
        private System.Windows.Forms.RadioButton warriorNoSubclassButton;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rogueButton;
        private System.Windows.Forms.RadioButton burglarButton;
        private System.Windows.Forms.RadioButton noSubclassThiefButton;
        private System.Windows.Forms.Button mageButton;
        private System.Windows.Forms.Button warriorButton;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button thiefButton;
        private System.Windows.Forms.RadioButton noSubclassMageButton;
        private System.Windows.Forms.GroupBox defaultConstr;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox extraConstruct;
        private System.Windows.Forms.Label lblSpecialAbility;
        private System.Windows.Forms.TextBox NameConst;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.RichTextBox displayCharactertextBox;
        private System.Windows.Forms.NumericUpDown armorUpDown;
        private System.Windows.Forms.NumericUpDown manaUpDown;
        private System.Windows.Forms.NumericUpDown HPUpDown;
        private System.Windows.Forms.PictureBox pictureCharacter;
        private System.Windows.Forms.Label lblspcAbility;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblPointsLeft;
        private System.Windows.Forms.Button btnStartOver;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCharacterpage;
    }
}

