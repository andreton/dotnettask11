﻿namespace DotNetTask11
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.syncDatabase = new System.Windows.Forms.Button();
            this.datagridCharacters = new System.Windows.Forms.DataGridView();
            this.deleteItemBtn = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnMainPage = new System.Windows.Forms.Button();
            this.TBoxNewValue = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.datagridCharacters)).BeginInit();
            this.SuspendLayout();
            // 
            // syncDatabase
            // 
            this.syncDatabase.Location = new System.Drawing.Point(130, 587);
            this.syncDatabase.Name = "syncDatabase";
            this.syncDatabase.Size = new System.Drawing.Size(112, 34);
            this.syncDatabase.TabIndex = 1;
            this.syncDatabase.Text = "Show all";
            this.syncDatabase.UseVisualStyleBackColor = true;
            this.syncDatabase.Click += new System.EventHandler(this.syncDatabase_Click);
            // 
            // datagridCharacters
            // 
            this.datagridCharacters.ColumnHeadersHeight = 50;
            this.datagridCharacters.Location = new System.Drawing.Point(49, 232);
            this.datagridCharacters.Name = "datagridCharacters";
            this.datagridCharacters.RowHeadersWidth = 100;
            this.datagridCharacters.Size = new System.Drawing.Size(1001, 319);
            this.datagridCharacters.TabIndex = 0;
            this.datagridCharacters.Text = "dataGridView1";
            // 
            // deleteItemBtn
            // 
            this.deleteItemBtn.Location = new System.Drawing.Point(380, 587);
            this.deleteItemBtn.Name = "deleteItemBtn";
            this.deleteItemBtn.Size = new System.Drawing.Size(112, 34);
            this.deleteItemBtn.TabIndex = 2;
            this.deleteItemBtn.Text = "Delete";
            this.deleteItemBtn.UseVisualStyleBackColor = true;
            this.deleteItemBtn.Click += new System.EventHandler(this.deleteItemBtn_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(588, 631);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(150, 34);
            this.btnUpdate.TabIndex = 3;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Crimson;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(225, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(678, 82);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome to the Dungeon master character library\r\n\r\n";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.LightSalmon;
            this.label2.Location = new System.Drawing.Point(12, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1057, 107);
            this.label2.TabIndex = 4;
            this.label2.Text = resources.GetString("label2.Text");
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(791, 631);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(112, 34);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnMainPage
            // 
            this.btnMainPage.Location = new System.Drawing.Point(791, 587);
            this.btnMainPage.Name = "btnMainPage";
            this.btnMainPage.Size = new System.Drawing.Size(112, 34);
            this.btnMainPage.TabIndex = 6;
            this.btnMainPage.Text = "Main page";
            this.btnMainPage.UseVisualStyleBackColor = true;
            this.btnMainPage.Click += new System.EventHandler(this.btnMainPage_Click);
            // 
            // TBoxNewValue
            // 
            this.TBoxNewValue.BackColor = System.Drawing.Color.LightSalmon;
            this.TBoxNewValue.Location = new System.Drawing.Point(588, 587);
            this.TBoxNewValue.Name = "TBoxNewValue";
            this.TBoxNewValue.Size = new System.Drawing.Size(150, 31);
            this.TBoxNewValue.TabIndex = 7;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LimeGreen;
            this.ClientSize = new System.Drawing.Size(1099, 710);
            this.Controls.Add(this.TBoxNewValue);
            this.Controls.Add(this.btnMainPage);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.deleteItemBtn);
            this.Controls.Add(this.syncDatabase);
            this.Controls.Add(this.datagridCharacters);
            this.Name = "Form2";
            this.Text = "Form2";
            ((System.ComponentModel.ISupportInitialize)(this.datagridCharacters)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView datagridCharacters;
        private System.Windows.Forms.Button syncDatabase;
        private System.Windows.Forms.Button deleteItemBtn;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnMainPage;
        private System.Windows.Forms.TextBox TBoxNewValue;
    }
}