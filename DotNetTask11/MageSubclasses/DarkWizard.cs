﻿using DotNetTask11.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask11
{
    class DarkWizard :Mage
    {
        public string specialAbility { get; set; }
        public DarkWizard(string name, int HP, int mana, int armorRating) : base(name, HP, mana, armorRating)
        {
            this.specialAbility = "Fireball";
        }
        public override string getSpecialAbility()
        {
            return specialAbility;
        }
        public override string ToString()
        {
            return "Name:" + this.name + " HP: " + this.HP + " Mana: " + this.mana + " Armor: " + this.armorRating + " Evil magic: " + this.specialAbility;
        }
        public override string description()
        {
            return "The dark wizard is a subclass of mage and has an extra secret ability (and anger issues)";
        }
        public override string characterClassType(){
            return "DarkWizard";
        }


    }
}
