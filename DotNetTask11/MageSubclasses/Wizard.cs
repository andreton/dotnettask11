﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask11
{
    class Wizard: Mage
    {
        public string specialAbility { get; set; }
        public Wizard(string name, int HP, int mana, int armorRating) : base(name, HP, mana, armorRating)
        {
            this.specialAbility = "Waterball";
        }
        public override string getSpecialAbility()
        {
            return specialAbility;
        }
        public override string ToString()
        {
            return "Name:" + this.name + " HP: " + this.HP + " Mana: " + this.mana + " Armor: " + this.armorRating + " Good magic: " + this.specialAbility;
        }
        public override string description()
        {
            return "The wizard is a subclass of mage and has an extra secret ability";
        }
        public override string characterClassType(){
            return "Wizard";
        }
    }
}
