﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask11
{
    class Squire : Warrior //Creates an subclass of Warrior with an overriden constructor with extra abilities
    {
        public string specialAbility { get; set; }
        public Squire(string name, int HP, int mana, int armorRating) : base(name, HP, mana, armorRating)
        {
            this.specialAbility = "speed";
        }
        public override string getSpecialAbility()
        {
            return specialAbility;
        }
        public override string ToString()
        {
            return "Name:" + this.name + " HP: " + this.HP + " Mana: " + this.mana + " Armor: " + this.armorRating + " Special ability: " + this.specialAbility;
        }
        public override string description()
        {
            return "The squire is a subclass of warrior and has an extra secret ability, he is very noble";
        }
        public override string characterClassType(){
            return "Squire";
        }
    }
}
