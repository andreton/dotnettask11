﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask11
{
    class Goblin : Warrior //Creates an subclass of Warrior with an overriden constructor with extra abilities
    {
        public string specialAbility { get; set; }
        public Goblin(string name, int HP, int mana, int armorRating) : base(name, HP, mana, armorRating)
        {
            this.specialAbility = "strength";
        }
        public override string getSpecialAbility()
        {
            return specialAbility;
        }
        public override string ToString()
        {
            return "Name:" + this.name + " HP: " + this.HP + " Mana: " + this.mana + " Armor: " + this.armorRating + " Special Ability: " + this.specialAbility;
        }
        public override string description()
        {
            return "The Goblin is a subclass of warrior, a ugly creature with an extra ability";
        }
        public override string characterClassType(){
            return "Goblin";
        }
    }
}
